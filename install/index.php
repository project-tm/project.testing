<?php

include (__DIR__ .'/../lib/user.php');

use Bitrix\Main\Application,
    Bitrix\Main\ModuleManager,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\Loader;

IncludeModuleLangFile(__FILE__);

class project_testing extends CModule {

    public $MODULE_ID = 'project.testing';

    function __construct() {
        $arModuleVersion = array();

        include(__DIR__ . '/version.php');

        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }

        $this->MODULE_NAME = Loc::getMessage('PROJECT_TESTING_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('PROJECT_TESTING_DESCRIPTION');
        $this->PARTNER_NAME = Loc::getMessage('PROJECT_TESTING_PARTNER_NAME');
        $this->PARTNER_URI = '';
    }

    public function DoInstall() {
        ModuleManager::registerModule($this->MODULE_ID);
        RegisterModuleDependences('main', 'OnProlog', $this->MODULE_ID, "Project\Testing\Event", "OnProlog");
        Loader::includeModule($this->MODULE_ID);
    }

    public function DoUninstall() {
        Loader::includeModule($this->MODULE_ID);
        ModuleManager::unRegisterModule($this->MODULE_ID);
    }

    protected function GetConnection() {
        return Application::getInstance()->getConnection(LogsTable::getConnectionName());
    }

}
